Scraper module readme.txt
by David Donohue (dado@drupal.org)
14 February 2006

OVERVIEW
This module scrapes data from web pages.  This data could be imported into a Drupal site as nodes or used for 
any other purpose.  

To scrape data from a web page, an administrator creates a "scraper job", which she configures to point 
to the URLs of interest.  She adds form field settings to permit the scraper to traverse logins, pagination, etc.  
She adds instructions as to how to find the fields of data for each record (node), using a combination of PHP 
and XPath.  The module includes a library of regular expression functions to permit extraction of phone 
numbers postal codes, dates, times, etc.  Scraper is capable of complex functionality, including the ability 
to submit forms with dynamic form info, and the ability for jobs to fire off subjobs.  

REQUIREMENTS
Drupal 4.7
PHP5
with XML DOM - http://www.zend.com/php5/articles/php5-xmlphp.php#Heading7
Tidy - http://www.zend.com/php5/articles/php5-tidy.php

DISCLOSURE
(1) Web scraping is never easy.  It can take numerous hours to learn how to make a web scraper work.
(2) Some PHP experience is required, as is the ability to learn XPath.
(3) Your working scraper job will break when the web page you are trying to scrape changes its fundamental structure.
(4) Some sites (with complex Javascript, poorly delimited data, or other quirks) might prove impractical to scrape.
(5) RSS or similar, when available, obviates the need for scraping.
(6) If you use this module you will likely discover 1 or more bugs in this module.

SECURITY
Note that this module permits entry of PHP code via a web interface.  Thus, it exposes your site to potential attacks.
Be sure to grant nobody besides yourself permission to "administer scraper" or "administer access control".  
It is also recommended that you disable this module when not in use.

When possible, deploy this module only on a localhost or private environment, 
for security reasons.  At this writing, this module is only capable of generating a 
CSV file, which you could then import into your production site.

See this excellent summary on Drupal security
http://www.webschuur.com/node/481

LEGAL
Like your browser's "copy and paste" functionality, this module can be used to pull copyrighted material 
from another site.  It is intended to be used in compliance with any copyright restrictions.  Be sure to follow 
all license restrictions of any target website.

HOW TO
Data extraction is done by scraper jobs.  You can create and configure scraper jobs at administer -> scraper.
Click "create a new job".  At a minimum, you must give your scraper job a distinct name and 1 or more URLs to scrape.
Under "Scraping Settings" are the rules you will need ot extract data.  Under "Form Settings" are the settings
for handling login, posting forms, pagination.

I will run through an example use of this module.  
Say you want to import US federal job postings into your site as nodes.  Who doesn't?

URLs to scrape
http://jobsearch.usajobs.opm.gov/jobsearch.asp

Enter maximum number of URLs to follow:
3

This tells scraper to scrape the first 3 pages of results.

Instructions to extract page values:
page_title=xpath://head/title
_next_relative_url=xpath://td[@align="right" and @class="MNSPagination"]/a[@class="MNSPagination"]/@href
next_url=php:scraper_get_absolute_url($source_url,$pagevals["_next_relative_url"])

These values populate a PHP array called $pagevals.  Note that the 3rd instuction ("next_url") 
refers to the earlier page value param "_next_relative_url" as $pagevals["_next_relative_url"].

There are 2 types of instructions.  Every instruction will start with either "xpath:" or "php:" to indicate which type.
"php" instructions consist of a single line of PHP code (without semicolon), which will be evaluated & executed.
"xpath" instructions consist of xpath describing the path RELATIVE to the current record's reference node.

The page value parameter "next_url" has special meaning to scraper.  When a value is defined for this parameter, 
it will use this value as the URL to follow for the next page of results.  It will ignore any entries in the 
"URLs to scrape" field after the first.

XPath to define records of data within the HTML:
//table[@id="JobSearchResultsHeader"]/tr[position()>1]

This setting tells the XPath which will define each record of data (i.e. each future Drupal node).  
The scraper engine will employ this XPath to define the set of DOM nodes to use as REFERENCE NODES in the DOM.
So the above instruction will define the first record's reference node to be the 2nd <tr> tag following the <table> tag
whose id attribute = "JobSearchResultsHeader".  The next record will be the 3rd <tr> tag.  The next record will be
the 4th <tr> tag.  And so on.  For each record, field values are pulled relative to the current record's 
reference node.

Instructions to extract field values from each record:
job_closing_date=xpath:td[1]
job_title=xpath:td[2]
_relative_job_url=xpath:td[2]/a/@href
job_url=php:scraper_get_absolute_url($source_url,$vals["_relative_job_url"])
agency=xpath:td[3]
location=xpath:td[4]
salary=xpath:td[5]

These values populate a PHP array called $vals.  They define the fields for each record within the recordset.  
They use identical instruction syntax to the page values parameters (above).

Field vals whose name begin with an underscore are not added to the final data output.  
They are intended to be used internally by later field instructions.

Note the above example field value parameter "job_url" uses a built-in helper function 
scraper_get_absolute_url(), which takes as a first argument a variable $source_url.  
Scraper populates the variable $source_url with the current URL.

Click "Save" to store your changes.  "Save as new" creates a new job with the current settings.  
You must give a job a distinct name before saving as new.

The buttons under "Scraper Operation" are how you make scraper operate.

"display page" saves job settings and opens a new browser window to the first URL in "URLs to scrape"
"display xml" saves job settings and gets HTML from the first URL 
[GETing or POSTing any "Default, static form parameters to send to each page" configured under "Form Settings"],
 tidies into XML, and displays this in a new browser.
"test run" saves job settings and gets tidied XML as above, then applies your scraping rules.
It outputs the results of these attempts in a new browser window.
"generate csv" does the same thing as "test run" except it only outputs the resulting CSV text to the browser.  
You can save this CSV by going to browser's File -> save option. [Note that you might have trouble with 
Firefox's File -> save operation, so if you are unable to get Firefox to save the web page as text, then
you might have to use Internet Explorer.]

When constructing a scraper job, I recommend you click these buttons in succession (as listed above).  
It is always helpful to see the actual page you are scraping.  
Next, view the (tidied) XML source, and plan your extraction strategy.  
Determine any page values you might want to extract and use (such as page title).
Determine the XPath expression to use, which will define the record's reference nodes.  
Test your settings frequently by clicking the "test run" button.

HELPER FUNCTIONS
Here are some helper functions you can use in your scraping.  They all use regular expressions to 
extract some string of interest from a larger string (usually the latter string retrieved using an XPath
instruction).  They are invoked by using php instructions.

/** Extracts US or Canadian phone from a string of text 
*/
scraper_extract_usca_phone($str, $default="")

/** Extracts US Zip code from string of text */
scraper_extract_US_zipcode($str, $default="")

/** Gets all text before a given string marker within a string of text 
*/
function scraper_get_text_before($str, $marker, $default="")

/** Gets all text after a given string marker within a string of text 
*/
function scraper_get_text_after($str, $marker, $default="")

/** Retrieves a start time (or a simple time) from a string of text.
 * Matches 6 A.M., 12:30 pm, noon, midnight, etc. 
 */
function scraper_get_start_time($str, $default="")

/** Retrieves an end time (or a simple time) from a string of text. Matches 6-8
 * A. M., 9:30 to mignight, noon through 2pm, etc. 
 */
function scraper_get_end_time($str, $default="")

/** takes a date string plus a time string and reurns a datetime (number)
 * representing the UNIX time 
 */
function scraper_get_start_date_time($datestr,$timestr)

/** given the string representing the end date and the end time of an event, plus
 * the (numeric) UNIX time of the start of the event and a default duration
 * (should an end date or end time be unavailable), retrieves a UNIX time
 * representing the end time of an event.  Default default_duration = 1 hour 
 */
function scraper_get_end_date_time($enddatestr,$endtimestr="",$startdatetime_int,$default_duration = 3600)

/** given a base URL and a relative URL, constructs an absolute URL.  Useful for
 * extracting an absolute URL when an <a> or <img> tag uses only relative URL
 * 
 * Example usage:
 * my_absolute_url=php:scraper_get_absolute_url($source_url,$vals["_relative_url"])
 * $source_url always contains the URL of the current page */
function scraper_get_absolute_url($absolute_url,$relative_url)


ADVANCED SCRAPING
This module is capable of more advanced scraping.  It can submit forms, call scraper jobs 
which call other scraper jobs, which call other scraper jobs, and so on.
Briefly, for a job to invoke another job (a "subjob"), you can use this sort of instruction.
_place_name_in_form=xpath:/@value
_params_str=php:"parkname=".$vals["_place_name_in_form"]
_add_records=php:scraper_subjob(62, $vals["_params_str"])

The parameter "_add_params" is a dummy parameter, and is not populated.  
Rather, this instruction kicks off a subjob (whose Job_id = 62 in this case).  
It passes the string value of $vals["_params_str"] to the next job.  
This second argument of function scraper_subjob() is a pipe-delimited list of parameters, as in
param1=param 1's value|param2=param 2's value|...

To configure a scraper job which can submit a form, and can also submit subsequnt forms, follow this general
approach.  Most special settings for this purpose are configured under the "Form Settings" section.
For the first page, put any static form parameters to submit in the 
"Default, static form parameters to send to each page" area.
If subsequent pages must read values from the page and submit another form, then extract the values
in the "Instructions to extract page values" area.  You can take these values and populate the
proper form parameters with them in the "Dynamic form parameters to send" area.  In this area, the syntax
used is a simple 
page_value_param_name=form_param_name

When dynamic values are present, they will overwrite the default static pages, and the resulting parameters
will be GETed or POSTed to the next page.  Whether to use GET or POST is another setting in the "Form Settings"
section.

FUTURE DIRECTIONS
Change subjobs to use job title instead of job_id, permitting transport of jobs which include subjobs across sites.
Enable scraping jobs to execute and import nodes as a cron process.  
Expanded documentaiton to illustrate recursive scraping and other advanced techniques.

REFERENCES
XPath: http://www.w3.org/TR/xpath


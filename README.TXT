As currently written, this module REQUIRES the Block cache module:

http://drupal.org/project/blockcache

It will technically run without it, but it is likely to drag your site, the site you're scraping a page from, and quite possibly the entire Internet down to a UMass-Amherst TAU-like crawl.

After enabling and configuring scraper, immediately set the original block to disabled and set up the cached block with a reasonable interval.
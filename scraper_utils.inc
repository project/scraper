<?php
/**
 * given a base URL and a relative URL, constructs an absolute URL.  Useful for
 * extracting an absolute URL when an <a> or <img> tag uses only relative URL
 * 
 * Example usage:
 * my_absolute_url=php:scraper_get_absolute_url($source_url,$vals
 * ["_relative_url"])
 * $source_url always contains the URL of the current page
 */
function scraper_get_absolute_url($absolute_url,$relative_url) {
  $lastslashpos = strrpos($absolute_url,"/");
  if (! $lastslashpos) return $absolute_url . $relative_url;
  $trimmed_abs_url = substr($absolute_url, 0, $lastslashpos);
  return $trimmed_abs_url . $relative_url;
}

/**
 * Gets all text before a given string marker within a string of text
 */
function scraper_get_text_before($str, $marker, $default="") {
  if (!$marker) return $str;
  $pos = strpos($str, $marker);
  if ($pos === false) return $default;
  $returnstr = substr($str, 0, $pos);
  return $returnstr;
}

/**
 * Gets all text after a given string marker within a string of text
 */
function scraper_get_text_after($str, $marker, $default="") {
  $pos = strpos($str, $marker);
  if ($pos === false) return $default;
  $returnstr = substr($str, $pos + strlen($marker), strlen($str));
  return $returnstr;
}

/**
 * reg expression to extract US/Canada phone number from a string.
 * First uses most restrictive method regexp, then less so, finally 
 * least restrictive.
 */
function scraper_extract_usca_phone($str, $default="") {
  if (!$default) $default="";
  
  //Setup Regexp strings
  $regexp_most_restrictive="/\\(?(\\d\\d\\d)?\\)?\\s*[\\.-]?\\s?(\\d\\d\\d)\\s?[\\.-]?\\s?(\\d\\d\\d\\d)/";
  $regexp_medium_restrictive="/[\\(A-Z0-9]{1}[A-Z0-9]{2}[A-Z0-9\\)\\- ]{2}[\\) \\-A-Z0-9]{2,10}/";
  $regexp_least_restrictive = "/[\\(\\) \\-A-Z0-9]{7,15}/";
  
  //try to match using most restrictive method
  $matchA = array();
  preg_match($regexp_most_restrictive,$str,$matchA);
  if ($matchA && count($matchA) >= 1) return $matchA[0];
  
  //if not successful, try to match using less restrictive method
  preg_match($regexp_medium_restrictive,$str,$matchA);
  if ($matchA && count($matchA) >= 1) return $matchA[0];
  
  //if not successful, try to match using less restrictive method
  preg_match($regexp_least_restrictive,$str,$matchA);
  if ($matchA && count($matchA) >= 1) return $matchA[0];
  
  //if not successful, return default
  return $default;
}

function scraper_extract_US_zipcode($str, $default="") {
  $regexp = "/\d{5}(\-?\d{4})?/";
  $matchA = array();
  preg_match($regexp,$str,$matchA);
  if ($matchA && count($matchA) >= 1) return $matchA[0];
  return $default;
}

/**
 * given the string representing the end date and the end time of an event, plus
 * the (numeric) UNIX time of the start of the event and a default duration
 * (should an end date or end time be unavailable), retrieves a UNIX time
 * representing the end time of an event.  Default default_duration = 1 hour
 */
function scraper_get_end_date_time($enddatestr,$endtimestr="",$startdatetime_int,$default_duration = 3600, $year='') {
  $endtimestr = trim(str_replace(".", "", $endtimestr));
  
  if (strlen($endtimestr) == 0) {//no end time has been specified
    $enddatetime_int = $startdatetime_int + $default_duration;
  } else {//an end time has been specified
  
    $datestr = trim($datestr);
    $regex = "/\b\d{4}\b/";
    $num_matches = preg_match($regex, $datestr);
    if ($num_matches == 0) {
      if (!strlen($year)) {
        $year = date('Y');
      }
      $datestr = $datestr . ' ' . $year;
    }
    $enddatetime = "$enddatestr $endtimestr";
    $enddatetime_int = strtotime($enddatetime);
    //if the end time is before the start time, assume we are dealing with a 9PM - 1AM type show.  Add 1 day to the end time.
    if ($startdatetime_int > $enddatetime_int) {
      $enddatetime_int = $enddatetime_int + 86400;
    }
  }
 
  return $enddatetime_int;
}

/**
 * changes noon & midnight to 12:00 PM & 12:00 AM respectively
 */
function scraper_noon_midnight_to_time($str) {
  $returnstr = $str;
  $returnstr = str_replace("-"," to ",$returnstr);
  $regexp = "[^[:alnum:]]noon($|[^[:alnum:]])";
  $returnstr = eregi_replace($regexp," 12:00 PM ",$returnstr);
  $regexp = "[^[:alnum:]]midn(ight|ite)($|[^[:alnum:]])";
  $returnstr = eregi_replace($regexp," 11:59 PM ",$returnstr);
  return $returnstr;
}

/**
* Finds AM or PM in the string, returns it if present otherwise  
returns null
*/
function get_am_pm($str) {
  $match = array();
  $regexp = "/(a|p|A|P)\.?\s?(m|M)\.?/";
  if ($str) preg_match($regexp,$str,$match);
  if ($match && is_array($match)) {
    $returnmatch = $match[0];
  } else {
    $returnmatch = null;
  }
  return $returnmatch;  
}

function remove_days_of_week($string) {
  $delete_from_str = array 
("monday","tuesday","wednesday","thursday","friday","saturday","sunday", 
"mon",
"tues","weds","wed","thurs","fri","sat","sun");
  foreach ($delete_from_str as $del) {
    $string = eregi_replace($del,"",$string);
  }
  return $string;
}

/**
* returns the complete time (including am or pm), or null if it is  
not a complete time
*/
function scraper_get_simple_time($str) {
  $matches = array();
  $regexp = "/\b((1[0-2])|[1-9])(:[0-5][0-9])?\s?(a|p|A|P)\.?\s?(m|M)?\.?\b/";

  preg_match_all($regexp,$str,$matches);
  return $matches;
}

function scraper_get_complete_time($str) {
  $matches = array();
  $regexp = "/((1[0-2])|[1-9])(:[0-5][0-9])?\s?(a|p|A|P)\.?\s?(m|M)?\.?/";
  preg_match_all($regexp,$str,$matches);
  if ($matches) $matches = $matches[0];
  return $matches;
}

/**
 * Formerly named get_dates.  Not sure how useful it is.
 */
function scraper_get_times($str) {
  $returnstr = "";
  
  $matches = array();
  $regexp = "/(((1[0-2])|[1-9])(:[0-5][0-9])?\s?(a|p|A|P)?\.?\s?(m|M)? 
\.?)\s*(to|thru|until|til|through)\s*(((1[0-2])|[1-9])(:[0-5][0-9])? 
\s?((a|p|A|P)\.?\s?(m|M)\.?))/";
  preg_match_all($regexp,$str,$returnstr,PREG_SET_ORDER);

  return $returnstr;
}

/**
 * Retrieves start date sans year (matches month & date only, as in March 13 or
 * Aug 2.
 */
function scraper_get_start_date($str) {
  $start_date = '';
  
  $thru_match = "/\b(to|thru|until|till|til|through)\b/i";
  $date_match = "/(\b(jan(uary)?|feb(ruary)?|mar(ch)?|apr(il)?|may|jun(e)?|jul(y)?|aug(ust)?|sep|sept|september|oct(ober)?|nov(ember)?|dec(ember)?)\s*(\d)+)/i";
  //$regexp = "([^(to|thru|until|til|through|\-)\s*](jan(uary)?|feb(ruary)?|mar(ch)?|apr(il)?|may|jun(e)?|jul(y)?|aug(ust)?|sep|sept|september|oct(ober)?|nov(ember)?|dec(ember)?)\s*(\d)+)";
  //$regexp = "((?<!((to|thru|until|til|through|\-)\s*))(jan(uary)?|feb(ruary)?|mar(ch)?|apr(il)?|may|jun(e)?|jul(y)?|aug(ust)?|sep|sept|september|oct(ober)?|nov(ember)?|dec(ember)?)\s*(\d)+)";
  $thru_matches = array();
  preg_match($thru_match,$str,$thru_matches, PREG_OFFSET_CAPTURE);
  $thru_position = -1;
  if ($thru_matches) {
    $thru_position = $thru_matches[0][1];
  }
  $date_matches = array();
  preg_match($date_match,$str,$date_matches, PREG_OFFSET_CAPTURE);
  $date_position = -1;
  if ($date_matches) {
    $date_position = $date_matches[0][1];
    if ($thru_position < 0 || $date_position < $thru_position) {
      $start_date = $date_matches[0][0];
    }
  }
  
  return $start_date;
}

function scraper_get_end_date($str) {
  $end_date = '';
  $month_match = "/(\b(jan(uary)?|feb(ruary)?|mar(ch)?|apr(il)?|may|jun(e)?|jul(y)?|aug(ust)?|sep|sept|september|oct(ober)?|nov(ember)?|dec(ember)?)\b)/i";
  $month_matches = array();
  preg_match_all($month_match, $str, $month_matches, PREG_OFFSET_CAPTURE);
  if ($month_matches) {
    //get the last month that appears
    $month_matches = $month_matches[0];
    $last_month = $month_matches[count($month_matches)-1][0];
    
    //begin to build the end date
    $end_date = $last_month;
    
    $last_month_position = $month_matches[count($month_matches)-1][1];
    
    //get the last date that appears after the last month
    //$next_start_position = $last_month_position + strlen($last_month);
    $next_start_position = $last_month_position;
    $last_date_str = substr($str, $next_start_position);
    
    //$date_match = "/[,-]?\s*(\d*)/";
    $date_match = "/(\b(jan(uary)?|feb(ruary)?|mar(ch)?|apr(il)?|may|jun(e)?|jul(y)?|aug(ust)?|sep|sept|september|oct(ober)?|nov(ember)?|dec(ember)?)\b)([,-\s]*\d{1,2}\b)+[,-\s]+(\d{4})?/i";
    //$date_match = "/[,-\s](\d{1,4})/";
    $date_matches = array();
    //preg_match_all($date_match, $str, $date_matches);
    preg_match_all($date_match, $last_date_str, $date_matches);

    if ($date_matches) {
      $last_date = $date_matches;
      
      $end_year = '';
      for ($i=count($last_date)-1; $i>0; $i--) {
        $date_num = trim($last_date[$i][0]);
        if (!is_numeric($date_num)) continue;
        
        if (strlen($date_num) == 4) {
          $end_year = $date_num;
          $end_date_num = $last_date[$i - 1][0];
          break;
        } else if (strlen($date_num) > 0 && strlen($date_num) < 3) {
          $end_date_num = $date_num;
          break;
        }
      }
      if (!$end_date_num || strlen($end_date_num)==0) return "";
      $end_date = $end_date . ' ' . $end_date_num;
      if (strlen($end_year) > 0) $end_date = $end_date . ', ' . $end_year;
    }
  }
  
  return $end_date;
}

/**
 * Retrieves start & end times, returns an array of matches
 */
function scraper_get_start_end_time($str,$config=PREG_PATTERN_ORDER) {
  $matches = array();
  $regexp = "/\b(((1[0-2])|[1-9])(:[0-5][0-9])?\s?(a|p|A|P)?\.?\s?(m|M)?\.?)\s*(to|thru|until|til|through)\s*(((1[0-2])|[1-9])(:[0-5][0-9])?\s?((a|p|A|P)\.?\s?(m|M)\.?))\b/";
  preg_match_all($regexp,$str,$matches,$config);
  //if ($matches) $matches = $matches[0];
  return $matches;
}

/**
 * Retrieves a start time (or a simple time) from a string of text.
 * Matches 6 A.M., 12:30 pm, noon, midnight
 */
function scraper_get_start_time($str, $default="") {
  $fixedstr = scraper_noon_midnight_to_time($str);
  $matches = scraper_get_start_end_time($fixedstr);
  $complete_matches = array();
  
  if (is_array($matches)) $start_matches = $matches[1];
  $start_match = "";
  if (is_array($start_matches) && count($start_matches)>0) {
    $start_match = $start_matches[0];
  } else {
    $start_match = $start_matches;
  }
  
  if ($start_match) {//did start_end time produce a result?  add am/pm if necessary
    $complete_matches = scraper_get_complete_time($start_match);
    if ($complete_matches) {
      if (is_array($complete_matches)) return 
$complete_matches[0];
      return $complete_matches;
    } else {
      //loop thru matches & find am or pm
      $match_am_pm = "";
      foreach ($matches as $match) {
        if (is_array($match)) $match = $match[0];
        $match_am_pm = get_am_pm($match);
        
        if ($match_am_pm) {
          return $start_match . " " . 
$match_am_pm;
        }
      }
    }
  } else {//try another approach
    $start_matches = scraper_get_simple_time($fixedstr);
    if ($start_matches) { 
      if (is_array($start_matches) && count($start_matches) > 0) $start_matches = $start_matches[0];
      if (is_array($start_matches) && count($start_matches) > 0) $start_matches = $start_matches[0];
      return $start_matches;
    }
  }
  return $default;
}

/**
 * Retrieves an end time (or a simple time) from a string of text. Matches 6-8
 * A. M., 9:30 to mignight, noon through 2pm, etc.
 */
function scraper_get_end_time($str, $default="") {
  $fixedstr = scraper_noon_midnight_to_time($str);
  $all_matches = scraper_get_start_end_time($fixedstr,PREG_SET_ORDER);
  $matches = array();
  //print "<h3>scraper_get_end_time() : all_matches=</h3>";
  //print_array($all_matches);
  if ($all_matches && is_array($all_matches)) {
    $matches = array_pop($all_matches);
    for ($i = count($matches)-1; $i >= 0; $i--) {
        $end_time = scraper_get_complete_time($matches[$i]);
        //print "<br>testing for complete time. found:";
        //print_array($end_time);
        if ($end_time) {
          if (is_array($end_time)) $end_time = $end_time[0];
          if (is_array($end_time)) $end_time = $end_time[0];
          return $end_time;
        }
    }
  }
  return $default;
  
}

/**
 * takes a date string plus a time string and reurns a datetime (number)
 * representing the UNIX time
 */
function scraper_get_start_date_time($datestr,$timestr,$year='') {
  $timestr = trim(str_replace(".", "", $timestr));
  $regex = "/\b\d{4}\b/";
  $datestr = trim($datestr);
  $num_matches = preg_match($regex, $datestr);
  if ($num_matches == 0) {
    if (!strlen($year)) {
      $year = date('Y');
    }
    $datestr = $datestr . ' ' . $year;
  }
  $datetime = "$datestr $timestr";
  return strtotime($datetime);
}

/**
 * Useful for debugging annoying line breaks or mystery characters.  Uncomment
 * line of code in function scraper_run() or elsewhere to use
 */
function scraper_get_char_vals($str) {
  $returnstr = "";
  $len = strlen($str);
  for ($i=0; $i<$len; $i++) {
    $char = substr($str, $i, 1);
    $returnstr .= '|' . $char . '=' . ord($char) . '|';
  }
  return $returnstr;
}
?>